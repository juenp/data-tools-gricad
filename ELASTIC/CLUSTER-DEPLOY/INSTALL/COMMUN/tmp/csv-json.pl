#!/usr/bin/perl
use strict;

my $filename;
my $line;
my $i=1;
my $filenameout;
my $file;
my $fileout;
my $separateur;
my @param; 
my @val;

if ($#ARGV != 1) {
    print "usage: csv-json fichier_csv separateur \n";
    exit;
}

$filename=$ARGV[0];
$separateur=$ARGV[1];

open $file, '<', $filename 
  or die "Fichier à charger non trouvé : $!\n";

$line= <$file>;
chomp($line);
@param=split /$separateur/,$line;
#print("\@Pametres lu: @param\n");
while( $line = <$file> ) {
	chomp($line);
	@val=split /$separateur/,$line;
	print ("{\"C0\":\"@val[0]\" , \"C1\":@val[1] , \"C2\":@val[2]}\n");
}
close($file);

print ("Terminé.\n");
