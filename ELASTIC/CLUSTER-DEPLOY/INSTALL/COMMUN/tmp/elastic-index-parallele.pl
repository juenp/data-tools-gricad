#!/usr/bin/perl

#$Config{useithreads} or die
#      "Recompilez Perl avec les threads activés pour faire tourner ce programme.";
      
      
use strict;

my $filename;
my $line;
my $i=1;
my $index_name;
my $index_type;
my $filenameout;
my $file;
my $fileout;

if ($#ARGV != 2) {
    print "usage: elasti-index filename.json index type \n";
    exit;
}

$filename=$ARGV[0];
$index_name=$ARGV[1];
$index_type=$ARGV[2];
$filenameout=`date +%y%m%d%s`;
chomp($filenameout);

open $file, '<', $filename 
  or die "Fichier à charger non trouvé : $!\n";
open $fileout, ">", $filenameout 
  or die "Fichier tampon non crée : $!\n";

while( $line = <$file> ) {
	chomp($line);
	print ($fileout "\{\"index\": \{\"_index\": \"$index_name\", \"_type\": \"$index_type\", \"_id\": \"$i\"\}\}\n");
        print ($fileout "$line\n");
	if ($i%400==0) {
	   if ($i%10000==0)  {
	      print ("Envoi de $i...\n");
	   }
	   close($fileout);
	   system ("(curl -s -XPUT localhost:9200/_bulk -H\"Content-Type: application/json\" --data-binary \@$filenameout >/dev/null ; rm $filenameout )\&");
	   $filenameout=`date +%y%m%d%s%N`;
           chomp($filenameout);
           open $fileout, '>', $filenameout
              or die "Fichier tampon non recrée : $!\n";
	}
	$i++;
}
close($file);
close($fileout);
my $a=`curl -XPUT localhost:9200/_bulk -H\"Content-Type: application/json\" --data-binary \@$filenameout; rm $filenameout`;
print ("$a\n");

print ("Terminé.\n");
