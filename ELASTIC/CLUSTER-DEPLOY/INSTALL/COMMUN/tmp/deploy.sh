swapoff -a

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
apt -y install apt-transport-https
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
apt update
apt -y upgrade

apt -y install htop screen
apt -y install curl
apt -y install default-jre nmap firefox
apt -y install elasticsearch 
apt -y install kibana logstash

mkdir /tmp/lib
/etc/init.d/elasticsearch stop
#cp /tmp/elasticsearch.yml /etc/elasticsearch/
cp -r /tmp/INSTALL/COMMUN/* /
cp -r /tmp/INSTALL/SERVEUR/* /
chown -R elasticsearch:elasticsearch /tmp/lib
chown -R elasticsearch:elasticsearch /etc/elasticsearch
systemctl daemon-reload
/etc/init.d/elasticsearch start

/etc/init.d/kibana stop
cp /tmp/kibana.yml /etc/kibana
/etc/init.d/kibana start

